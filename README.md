# Pysupervisor

Simple application that monitors given process and restart&notifies when the process stops working.

Written in **python3.5** using only standard python libraries.

### Example Usage:

#### To see all available options:

```$ python3.5 supervisor.py --help```

#### Watching process with pid=1234:

```$ SUPERVISOR_SMTP_PASSWORD=[smtp_password] python3.5 supervisor.py --check-interval 10 --pid 1234 --smtp-user user@example.com```

```$ tail -f supervisor.log```


#### Watching with disabled email-notifications
In case you don't have any external SMTP account that you may use you just can disable email notifications:

```$ python3.5 supervisor.py --check-interval 10 --pid 1234 --disable-email-notifications```

Author: *Krzysztof Szarlej*
