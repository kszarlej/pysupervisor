#!/usr/bin/env python3
import logging
import argparse
import subprocess
import sys
import time
import smtplib
import os
import socket

parser = argparse.ArgumentParser(description='Runtime Configuration')
parser.add_argument('--pid', type=int, required=True,
                    help='PID of program that will be monitored.')
parser.add_argument('--check-interval', type=int, default=30,
                    help='How often processes aliveness will be checked.')
parser.add_argument('--retry-interval', type=int, default=5,
                    help='When process will disappear try to restart it every RETRY_INTERVAL.')
parser.add_argument('--max-retries', type=int, default=4,
                    help='Maximum tries of process restart.')
parser.add_argument('--smtp-server', default='smtp.gmail.com',
                    help='SMTP Server address.')
parser.add_argument('--smtp-port', type=int, default='587',
                    help='SMTP Server port.')
parser.add_argument('--smtp-user', default='test@example.com',
                    help='SMTP Server user.')
parser.add_argument('--recipient', default='test@example.com',
                    help='Notifications receiver')
parser.add_argument('--disable-email-notifications', action='store_true')
parser.add_argument('--logfile', default="./supervisor.log",
                    help="Logfile path")


args = parser.parse_args()
process_pid = args.pid

logging.basicConfig(filename=args.logfile, level=logging.INFO)
logger = logging.getLogger(__name__)

# keep it in env so it is not visible in cmdline displayed by `ps`, `top` etc..
smtp_password = os.getenv("SUPERVISOR_SMTP_PASSWORD")


def email_notification(smtp_server, smtp_user, smtp_port, smtp_password, recipient, message):
    """ Sends email using external SMTP server. Only STARTTLS is supported. """
    if args.disable_email_notifications:
        logger.info("Skipping email notification.")
        return None

    my_hostname = socket.gethostname()

    message = "\r\n".join([
        "From: supervisor@{0}".format(my_hostname),
        "To: {0}".format(recipient),
        "Subject: [ ALERT ] Supervisor on {0}".format(my_hostname),
        "",
        "{0}".format(message)
    ])

    try:
        smtpconn = smtplib.SMTP(smtp_server, smtp_port)
        smtpconn.ehlo()
        smtpconn.starttls()
        smtpconn.login(smtp_user, smtp_password)
        smtpconn.sendmail(smtp_user, recipient, message)
        smtpconn.quit()
    except smtplib.SMTPAuthenticationError:
        logger.error("Cannot authenticate to SMTP server. Unable to send email notification. Check your credentials.")
        return None
    except:
        logger.error("Couldnt send email. Check your SMTP settings.")
        return None

    logger.debug("Successfully sent notification email.")


def get_cmdline(pid):
    """
        Gets commandline for a given id using `ps` utility
        We need it to restart application when it fails.
    """

    try:
        ps = subprocess.Popen(["/bin/ps", "-p", str(pid), "-o", "command"], stdout=subprocess.PIPE)
        try:
            outs, _ = ps.communicate(timeout=5)
        except subprocess.TimeoutExpired:
            logger.error("Timeout expired on ps.")
            ps.kill()
            ps.wait()
            return None

        logger.debug("ps command return code: %i", ps.returncode)

        if ps.returncode == 0:
            return outs.decode("utf-8").split("\n")[1]  # split to return data without COMMAND header
        else:
            logger.error("ps returned with non-zero status.")
            return None
    except OSError:
        logger.error("Couldnt get cmdline of process with PID=%s", pid)
        return None


def probe_process_aliveness(pid, cmdline):
    """
        Checks whether the process with given pid is alive.
    """
    logger.info("Probing PID %i for aliveness.", pid)
    current_cmdline = get_cmdline(pid)
    # if that pid exists
    if get_cmdline(pid):
        # and its cmdline is same as cmdline of process we are watching
        if current_cmdline == cmdline:
            # then we assume the process is healthy
            logger.debug("Current cmdline of process with pid %i equals cmdline of the watched process.", pid)
            return True

    return False


def start_process(cmdline):
    """ Starts new independent process. """
    args = []
    [args.append(x) for x in cmdline.split(" ")]
    logger.debug("Trying to start process with command: %s", cmdline)

    try:
        proc = subprocess.Popen(args, start_new_session=True)
        return proc.pid
    except IOError:
        logger.debug("Restarting process failed.")
        return None



def supervisor():
    """ Main function of the supervisor app """

    global process_pid
    logger.info("Program is starting..")
    cmdline = get_cmdline(process_pid)

    if not smtp_password and not args.disable_email_notifications:
        logger.fatal("""Please set SUPERVISOR_SMTP_PASSWORD environment variable
                      with correct SMTP password for SMTP configuration specified in positional arguments.
                      Use -h for additional help.""")
        sys.exit(1)

    if not cmdline:
        logger.fatal('Couldnt get cmdline of process with PID: %i', process_pid)
        sys.exit(1)
    else:
        logger.info('Successfully got cmdline of process with PID %i: %s', process_pid, cmdline)

    # Main loop in program.
    while True:
        alive = probe_process_aliveness(process_pid, cmdline)
        if not alive:
            logger.info("Process is dead. Trying to restart it.")

            email_notification(args.smtp_server, args.smtp_user, args.smtp_port, smtp_password,
                               args.recipient, "Process `{0}` is dead. Trying to restart it.".format(cmdline))
            new_pid = None
            retry = 1
            while new_pid is None:
                if retry > args.max_retries:
                    logger.error("max_retries number exceeded. exitting.")
                    email_notification(args.smtp_server, args.smtp_user, args.smtp_port, smtp_password,
                                       args.recipient, "max_retries number exceeded for application `{0}`. Application is down.".format(cmdline))
                    sys.exit(1)

                if retry != 1:
                    time.sleep(args.retry_interval)

                logger.info("Trying to restart application. Retry %i/%i", retry, args.max_retries)
                new_pid = start_process(cmdline)
                retry += 1

            logger.info("Successfully restarted process with new pid %i after %i retries", new_pid, retry - 1)
            email_notification(args.smtp_server, args.smtp_user, args.smtp_port, smtp_password,
                               args.recipient, "Successfully restarted process: `{0}` after {1} retries.".format(cmdline, retry - 1))
            process_pid = new_pid
        else:
            logger.info("Process is alive :)!")

        time.sleep(args.check_interval)


if __name__ == "__main__":
    # Fire supervisor as daemon and exit
    newpid = os.fork()
    if newpid == 0:
        # "Detach" from parent
        os.chdir("/")
        os.umask(0)
        os.setsid()
        # Run supervisor
        supervisor()
    else:
        logging.info("Supervisor started. See logfile under: %s", args.logfile)
        os._exit(0)
